
exception ListLengthMismatch

(* don't do this *)
fun old_zip3 (l1, l2, l3) =
    if null l1 andalso null l2 andalso null l3
    then []
    else if null l1 orelse null l2 orelse null l3
    then raise ListLengthMismatch
    else 
        (hd l1, hd l2, hd l3) :: old_zip3 (tl l1, tl l2, tl l3)

(* don't do this *)
fun shallow_zip3 (l1, l2 ,l3) = 
    case l1 of 
        [] => 
            (case l2 of 
                [] =>
                    (case l3 of 
                        [] => []
                        | _ => raise ListLengthMismatch)
                | _ => raise ListLengthMismatch)
        | hd1 :: tl1 => 
            (case l2 of 
                [] => raise ListLengthMismatch
                | hd2 :: tl2 => 
                    (case l3 of 
                        [] => raise ListLengthMismatch
                        | hd3 :: tl3 => 
                            (hd1, hd2, hd3) :: shallow_zip3 (tl1, tl2, tl3)))

(* do this *)
fun zip3 last_triple = 
    case last_triple of 
        ([], [], []) => []
        | (hd1 :: tl1, hd2 :: tl2, hd3 :: tl3) => 
            (hd1, hd2, hd3) :: zip3 (tl1, tl2, tl3)

fun unzip3 lst = 
    case lst of 
        [] => ([], [], [])
        | (x, y, z) :: tl => 
            let 
                val (xs, ys, zs) = unzip3 tl
            in 
                (x :: xs, y :: ys, z :: zs)
            end
            
(* section 2 more nested patterns *)
fun nondecreasing xs = (* int list -> bool *)
    case xs of 
        [] => true
        | [x] => true
        | x :: y :: tl => x <= y andalso nondecreasing (y :: tl)

datatype sgn = P | N | Z

(* int * int -> sign  *)
fun multsign (x1, x2) =
   let fun sign x = if x < 0 then N else if x > 0 then P else Z
   in 
        case (sign x1, sign x2) of 
              (Z, _) => Z
            | (_, Z) => Z
            | (P, P) => P
            | (N, N) => P
            | (P, N) => N
            | (N, P) => N
   end

fun len xs = 
    case xs of 
        [] => 0
        | _ :: tl => 1 + len tl
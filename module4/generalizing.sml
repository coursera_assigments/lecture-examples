datatype exp = Constant of int 
             | Negate of exp
             | Add of exp * exp
             | Multiply of exp * exp

fun true_of_all_constants (f,e) =
    case e of 
        Constant i => f i
        | Negate exp => true_of_all_constants(f, exp)
        | Add (exp,exp1) => true_of_all_constants(f, exp) andalso true_of_all_constants(f,exp1)
        | Multiply (exp,exp1) => true_of_all_constants(f, exp) andalso true_of_all_constants(f,exp1)

fun all_even e = true_of_all_constants(fn x => x mod 2 = 0, e)

val test = all_even(Add(Constant 2,Constant 3))



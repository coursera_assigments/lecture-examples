
fun pow(x: int , y : int) = 
    if y = 0
    then 1
    else x * pow(x, y - 1)

val x = (2,3)
val ans = pow x


fun list_product ( xs : int list ) = 
    if null xs
    then 1
    else hd xs * list_product(tl xs)

val x = [2,3,4]

val ans = list_product x

fun count_down (x : int) = 
    if x = 0
    then []
    else x :: count_down(x - 1)

count_down(7)

fun append (xs : int list , ys : int list) = 
    if null xs
    then ys
    else (hd xs) :: append((tl xs) , ys)

val x = [3,5];
val y = [4,2];
val z = append(x,y);
val d = 1 :: y;
fun sum_pair_list(xs : (int * int) list) = 
    if null xs
    then 0 
    else #1 (hd xs) + #2 (hd xs) + sum_pair_list(tl xs)

val x = [(3,2), (2,4)];

sum_pair_list(x);


fun firsts (xs : (int * int) list) = 
    if null xs 
    then [] 
    else #1 (hd xs) :: firsts(tl xs)

val x = [(3,2), (2,4)];
firsts(x);

fun seconds (xs : (int * int) list)  =
    if null xs 
    then [] 
    else #2 (hd xs) :: seconds(tl xs)

val x = [(3,2), (2,4)];

seconds(x)

fun test (x: int, y : int , z : int  ) = 

    x
